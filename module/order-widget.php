<div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-body text-center">
                    <h3>Order <b>Free Tickets</b></h3>
                    <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <div class="col-sm-12">
                          <input type="email" class="form-control" name="firstname" placeholder="First Name">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-12">
                          <input type="password" class="form-control" name="lastname" placeholder="Last name">
                        </div>
                      </div>
                        <div class="form-group">
                        <div class="col-sm-12">
                          <input type="password" class="form-control" name="email" placeholder="Email Address"/>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-4">
                          <button type="submit" class="btn btn-default">Order</button>
                        </div>
                      </div>
                    </form>
                </div>
        </div>
    </div>
</div>