<nav class="navbar navbar-default" role="navigation">
    <div class="container nav-shrink">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>

          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          <a class="navbar-brand home" href="#"><span class="glyphicon glyphicon-home"></span></a>

      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
              <li><a href="#">Exhibitor List</a></li>
              <li><a href="#">Exhibiting</a></li>
              <li><a href="#">Partners</a></li>
              <li><a href="#">About Us</a></li>
              <li><a href="#">Visiting</a></li>          
              <li><a href="#">Blog</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right hidden-sm">
              <li class="active"><a href="#">Order Free Tickets</a></li>
          </ul>
          </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  
</nav>