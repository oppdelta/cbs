<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TDA - Responsive About</title>

    <!-- Bootstrap -->
    <style>
        @import url('css/bootstrap.min.css');
        @import url('css/style.css');
    </style>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

</head>
<body>

<?php
//Include the navbar
include_once 'module/nav.php';
?>

<div class="container">
    <h1>Contact Us</h1>
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    Call us
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    Visit Us
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Parking's not a problem - we have plenty of spaces in our secure private car park situated right outside the office.</p>

                    <p>Getting here by train is easy. Cheltenham Spa, located on the main line between Birmingham and Bristol. Once here you can either take a taxi, we recommend Starline on 01242 250250 or take a casual stroll down the historic Honeybourne line which leads directly to our office.</p>
                </div>
            </div>
        </div>
    </div>

    <h1>Request a callback</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Leave us your name and number and we'll get back to you as quickly as we can. (* denotes required field)</p>
                    <div class="row">
                        <form class="form-inline" role="form">

                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail2">Name:</label>
                                    <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Name">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputPassword2">Phone:</label>
                                    <input type="password" class="form-control" id="exampleInputPassword2" placeholder="07123456789">
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="submit" class="btn btn-info">Call me back</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>


