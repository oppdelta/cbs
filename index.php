<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cafe Business - Responsive</title>

    <!-- Bootstrap -->
    <style>
    @import url('css/bootstrap.css');
    @import url('css/style.css');
    </style>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
        $(function() {
            $(".tip").tooltip();
        })
    </script>
  </head>
  <body>
  
      
<div class="container">
        <div class="row">
            <div class="col-sm-6  col-md-5 text-center">
                <img src="imgs/cb-logo.png" />
            </div>
            <div class="col-md-2 hidden-xs hidden-sm">
                <img class="home social" src="imgs/linkedin.png" />
                <img class="home social" src="imgs/twitter.png" />
            </div>
            <div class="col-sm-6 col-md-5 text-center">
                <div class="hidden-xs">
                      <h2>17 & 18 SEP 2014</h2>
                      <h2 class="text-muted">Olympia LONDON</h2>
                </div>
                <div class="hidden-sm hidden-md hidden-lg">
                      <h2 style="color: #fff">17 & 18 SEP 2014</h2>
                      <h2 class="text-muted">Olympia LONDON</h2>
                </div>
            </div>
        </div>
          
</div>
      <?php
              //Include the navbar
              include_once 'module/nav.php';
    ?>
     
<div class="container content">
            <div class="row">

                <div class="col-sm-4">
                    <div class="list-group">
                            <a href="#" class="list-group-item visible-sm text-center" style="background-color:#c76616; color:#fff;">Order Free Tickets</a>
                            <a href="#" class="list-group-item list-group-item-info hidden-sm">Order Free Tickets</a>
                        
                            <a href="#" class="list-group-item active">Keynote Speakers</a>
                            <a href="#" class="list-group-item">Seminar Hall 1</a>
                            <a href="#" class="list-group-item">Seminar Hall 2</a>
                            <a href="#" class="list-group-item">Seminar Hall 3</a>
                            <a href="#" class="list-group-item">Marketing Workshop</a>
                            <a href="#" class="list-group-item">Cafe tech Workshop</a>
                            <a href="#" class="list-group-item">Coffee Art Workshop</a>
                        </div>
                   
                    <h5>Cafe Business Innovation is an exciting new exhibition.</h5>
                    <p>
Running in conjunction with Takeaway Expo, the UK's largest event of it's kind. The show offers a unique blend of seminars, workshops, exhibitors, and networking to support and inspire more than 3,000 visitors. This is you opportunity to visit or exhibit at the very first Cafe Business Innovation on 17-18 September 2014 at Olympia London.</p>
                
                    <h5>Located in partnership with</h5>
                    <p class="text-center"><img src="imgs/cb-takeaway-logo.png" /></p>
                    <?php include 'module/order-widget.php'; ?>
                   
                </div>
                
                
<!--Right side -->
                <div class="col-sm-8">
                    <div class="hidden-xs">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                          </ol>

                          <!-- Wrapper for slides -->
                          <div class="carousel-inner">
                              <div class="item active"><img src="imgs/slide1.jpg" alt="Expert Tips"></div>
                              <div class="item"><img src="imgs/slide2.jpg" alt="Expert Tips"></div>
                              <div class="item"><img src="imgs/slide3.jpg" alt="Expert Tips"></div>
                          </div>

                          <!-- Controls -->
                          <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                          </a>
                          <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                          </a>
                        </div>
                        <div class="text-center carousel-footer">3,000 INTERNATIONAL BUSINESSES | <b>50 EXHIBITORS</b> |  47 SEMINARS | <b>7 WORKSHOPS</b> |  KEYNOTE SPEAKERS</div>
                    </div>
                    <div class="panel panel-info">
                        
                        <div class="panel-body row">
                            <div class="col-md-12">    
                            <h3>Speakers</h3>
                            </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="thumbnail">
                                        <img src="imgs/sem1.jpg" alt="...">
                                    </div>    
                                          <h3>Doug Richard</h3>
                                          <h5>Former Dragon and Business Expert</h5>
                                          <p>Former BBC Dragon and serial entrepreneur Doug Richard will take...</p>
                                        
                                    
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="thumbnail">
                                        <img src="imgs/sem2.jpg" alt="...">
                                        </div>
                                          <h3>Felicity McCarthy</h3>
                                          <h5>Facebook's Head of SMB Marketing Communications</h5>
                                          <p>Using Facebook to drive sales for your growing business...</p>
                                        
                                   
                                </div>
                                <div class="clearfix visible-xs visible-sm"></div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="thumbnail">
                                        <img src="imgs/sem3.jpg" alt="...">
                                    </div>
                                          <h3>Tom Portesy</h3>
                                          <h5>How to franchise your cafe business</h5>
                                          <p>'The Franchising Expert' from the USA has helped hundreds of small</p>
                                    
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="thumbnail">
                                        <img src="imgs/sem4.jpg" alt="...">
                                    </div>
                                        <h3>Martyn Dawes</h3>
                                          <h5>The Incredible Coffee Nation Story</h5>
                                          <p>Martyn Dawes founded Coffee Nation in 1996 and sold it for...</p>
                                      
                                </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    
                                    <h4>50 Exhibitors</h4>
                                    <p>
                                        You'll find a wide selection of suppliers at Coffee Business Innovation, all gathered in one place so that you can select the very best products, solutions and services for your coffee business. Whether you want a new coffee machine, to find out more about green coffee, or the latest advice to grow your business, you'll find it all at Coffee Business Innovation.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    
                                    <h4>Seminars</h4>
                                    <p>
                                        Our comprehensive series of seminars covers a huge range of topics from making the perfect coffee to the latest technology, alongside business advice, offering you the very best seminars from key players in the industry. This unique seminar schedule is led by industry experts and thought leaders, who will share their insights and experience directly with you.
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4 hidden-xs hidden-sm">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    
                                    <h4>Exhibitors</h4>
                                    <p>
                                        <img src="imgs/cb-logo.png" class="img-responsive"/>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    
                                    <h4>Workshops</h4>
                                    <p>
                                        Pick up new skills and sample new products or services with each of our interactive workshops, led by industry experts.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    
                                    <h4>Live Features</h4>
                                    <p>
                                        Witness stunning coffee art by the masters, or compete to prove you are the fastest cup in the West.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            
                        </div>
                    </div>
                </div>
                
            </div>

</div>  
      
<div class="container-fluid footer">
    <div class="container">
      <div class="row ">
          <div class="col-sm-2">
              <div class="panel panel-primary">
                  <ul>
                    <li>> Home</li>
                    <li>> Order Free Tickets</li>
                    <li>> Features</li>
                    <li>> Exhibitor List</li>
                    <li>> Speakers</li>
                  </ul>
              </div>
          </div>
          <div class="col-sm-2">
              <div class="panel panel-primary">
                  <ul>
                    <li>> Travel/Directions</li>
                    <li>> Privacy Policy</li>
                    <li>> Prysm Group</li>
                    <li>> Terms & Conditions</li>
                  </ul>
              </div>
          </div>
          <div class="col-sm-2"></div>
          <div class="col-sm-6 text-right text-muted">
              Prysm BSU Limited, a company registered in the United Kingdom, with registered number 07447489 and with its registered office at 3rd Floor, Colston Tower, Colston Street, Bristol, UK, BS1 4UB.
<br /><br />
Copyright © 2009–2014 Prysm BSU Limited. All rights reserved.
          </div>
      </div>
    </div>
</div>  
</body>
</html>